/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Classes for the main app, as well as its runtime state and initial configuration.
*/

import SwiftUI
import Combine
import SoundAnalysis

/// Contains customizable settings that control app behavior.
struct AppConfiguration {
    /// Indicates the amount of audio, in seconds, that informs a prediction.
    var inferenceWindowSize = Double(1.5)

    /// The amount of overlap between consecutive analysis windows.
    ///
    /// The system performs sound classification on a window-by-window basis. The system divides an
    /// audio stream into windows, and assigns labels and confidence values. This value determines how
    /// much two consecutive windows overlap. For example, 0.9 means that each window shares 90% of
    /// the audio that the previous window uses.
    var overlapFactor = Double(0.95)

    /// A list of sounds to identify from system audio input.
    var monitoredSounds = Set<SoundIdentifier>()

    /// Retrieves a list of the sounds the system can identify.
    ///
    /// - Returns: A set of identifiable sounds, including the associated labels that sound
    ///   classification emits, and names suitable for displaying to the user.
    static func listAllValidSoundIdentifiers() throws -> Set<SoundIdentifier> {
        let labels = try SystemAudioClassifier.getAllPossibleLabels()
        return Set<SoundIdentifier>(labels.map {
            SoundIdentifier(labelName: $0)
        })
    }
}

/// The runtime state of the app after setup.
///
/// Sound classification begins after completing the setup process. The `DetectSoundsView` displays
/// the results of the classification. Instances of this class contain the detection information that
/// `DetectSoundsView` renders. It incorporates new classification results as the app produces them into
/// the cumulative understanding of what sounds are currently present. It tracks interruptions, and allows for
/// restarting an analysis by providing a new configuration.
class AppState: ObservableObject {
    /// A cancellable object for the lifetime of the sound classification.
    ///
    /// While the app retains this cancellable object, a sound classification task continues to run until it
    /// terminates due to an error.
    private var detectionCancellable: AnyCancellable? = nil

    /// The configuration that governs sound classification.
    private var appConfig = AppConfiguration()

    /// A list of mappings between sounds and current detection states.
    ///
    /// The app sorts this list to reflect the order in which the app displays them.
    @Published var detectionStates: [(SoundIdentifier, DetectionState)] = []

    @Published var detectionLog: [(DetectionEvent, Date)] = []

    /// Indicates whether a sound classification is active.
    ///
    /// When `false,` the sound classification has ended for some reason. This could be due to an error
    /// emitted from Sound Analysis, or due to an interruption in the recorded audio. The app needs to prompt
    /// the user to restart classification when `false.`
    @Published var soundDetectionIsRunning: Bool = false

    /// Begins detecting sounds according to the configuration you specify.
    ///
    /// If the sound classification is running when calling this method, it stops before starting again.
    ///
    /// - Parameter config: A configuration that provides information for performing sound detection.
    func restartDetection(config: AppConfiguration) {
        SystemAudioClassifier.singleton.stopSoundClassification()

        let classificationSubject = PassthroughSubject<SNClassificationResult, Error>()

        var soundOptions: Set<SoundIdentifier>
        do {
            soundOptions = try AppConfiguration.listAllValidSoundIdentifiers()
        } catch {
            soundOptions = Set<SoundIdentifier>()
        }

        soundOptions = soundOptions.filter({ si in
            si.labelName.contains("breath") ||
            si.labelName.contains("sigh") ||
            si.labelName.contains("cough") ||
            si.labelName.contains("gasp") ||
            si.labelName.contains("sneeze")
            //si.labelName.contains("snoring")
        })

        detectionCancellable =
          classificationSubject
          .receive(on: DispatchQueue.main)
          .sink(receiveCompletion: { _ in self.soundDetectionIsRunning = false },
                receiveValue: {
              let ds = AppState.advanceDetectionStates(self.detectionStates, givenClassificationResult: $0)
              self.updateDetectionLog(ds)
              self.detectionStates = ds
                })

        self.detectionStates =
          [SoundIdentifier](soundOptions)
          .sorted(by: { $0.displayName < $1.displayName })
          .map { ($0, DetectionState(presenceThreshold: 0.4,
                                     absenceThreshold: 0.3,
                                     presenceMeasurementsToStartDetection: 1,
                                     absenceMeasurementsToEndDetection: 50))
          }

        soundDetectionIsRunning = true
        appConfig = config
        detectionLog.removeAll()
        SystemAudioClassifier.singleton.startSoundClassification(
          subject: classificationSubject,
          inferenceWindowSize: config.inferenceWindowSize,
          overlapFactor: config.overlapFactor)
    }

    /// Updates the detection states according to the latest classification result.
    ///
    /// - Parameters:
    ///   - oldStates: The previous detection states to update with a new observation from an ongoing
    ///   sound classification.
    ///   - result: The latest observation the app emits from an ongoing sound classification.
    ///
    /// - Returns: A new array of sounds with their updated detection states.
    static func advanceDetectionStates(_ oldStates: [(SoundIdentifier, DetectionState)],
                                       givenClassificationResult result: SNClassificationResult) -> [(SoundIdentifier, DetectionState)] {
        let confidenceForLabel = { (sound: SoundIdentifier) -> Double in
            let confidence: Double
            let label = sound.labelName
            if let classification = result.classification(forIdentifier: label) {
                confidence = classification.confidence
            } else {
                confidence = 0
            }
            return confidence
        }
        return oldStates.map { (key, value) in
            (key, DetectionState(advancedFrom: value, currentConfidence: confidenceForLabel(key)))
        }
    }
    
    func updateDetectionLog(_ newStates: [(SoundIdentifier, DetectionState)]) {
        for st in newStates {
            if st.1.isDetected {
                if let stOld = (self.detectionStates.first { (si, ds) in
                    let confLvl = st.0.labelName.contains("breath") ? 0.35 : (st.0.labelName.contains("sigh") || st.0.labelName.contains("gasp") ? 0.25 : 0.6)
                    return si.labelName == st.0.labelName && ds.currentConfidence > st.1.currentConfidence && ds.currentConfidence > confLvl
                }) {
                    if stOld.0.labelName.contains("breath") {
                        if (detectionLog.isEmpty || (detectionLog.last?.0 != .breathIn && (detectionLog.last?.1.timeIntervalSinceNow ?? 0) < -1 )) {
                            detectionLog.append((.breathIn, Date.now))
                        }
                    } else if stOld.0.labelName.contains("sigh") || stOld.0.labelName.contains("gasp") {
                        if (detectionLog.last?.0 != .breathOut && (detectionLog.last?.1.timeIntervalSinceNow ?? 0) < -1.8 ) {
                            detectionLog.append((.breathOut, Date.now))
                        }
                    } else {
                        if (detectionLog.last?.1.timeIntervalSinceNow ?? -1) > -0.9 {
                            if !detectionLog.isEmpty {
                                detectionLog.removeLast()
                            }
                        }
                    }
                }
            }
        }
    }
}

@main
struct BreathDetectionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
