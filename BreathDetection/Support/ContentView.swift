/*
 See LICENSE folder for this sample’s licensing information.
 
 Abstract:
 The top-level view for the app.
 */

import AVFoundation
import Foundation
import MessageUI
import SwiftUI
import UIKit

/// The main view that contains the app content.
struct ContentView: View {
    /// A configuration for managing the characteristics of a sound classification task.
    @State var appConfig = AppConfiguration()
    
    /// The runtime state that contains information about the strength of the detected sounds.
    @StateObject var appState = AppState()
    
    @State var result: Result<MFMailComposeResult, Error>? = nil
    
    var body: some View {
        ZStack {
            DetectSoundsView(state: appState,
                             config: $appConfig)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


struct MailView: UIViewControllerRepresentable {
    @Environment(\.presentationMode) var presentation
    @Binding var result: Result<MFMailComposeResult, Error>?
    var recipients = [String]()
    var messageBody = ""
    var messageSubject = ""
    var doneAction: () -> Void

    class Coordinator: NSObject, MFMailComposeViewControllerDelegate {
        @Binding var presentation: PresentationMode
        @Binding var result: Result<MFMailComposeResult, Error>?
        var doneAction: () -> Void

        init(presentation: Binding<PresentationMode>,
             result: Binding<Result<MFMailComposeResult, Error>?>,
             doneAction: @escaping () -> Void)
        {
            _presentation = presentation
            _result = result
            self.doneAction = doneAction
        }
        
        func mailComposeController(_: MFMailComposeViewController,
                                   didFinishWith result: MFMailComposeResult,
                                   error: Error?)
        {
            defer {
                $presentation.wrappedValue.dismiss()
            }
            guard error == nil else {
                self.result = .failure(error!)
                return
            }
            self.result = .success(result)
            
            if result == .sent {
                doneAction()
                AudioServicesPlayAlertSound(SystemSoundID(1001))
            }
        }
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(presentation: presentation,
                           result: $result,
                           doneAction: doneAction)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<MailView>) -> MFMailComposeViewController {
        let vc = MFMailComposeViewController()
        vc.setToRecipients(recipients)
        vc.setSubject(messageSubject)
        vc.setMessageBody(messageBody, isHTML: false)
        vc.mailComposeDelegate = context.coordinator
        return vc
    }
    
    func updateUIViewController(_: MFMailComposeViewController,
                                context _: UIViewControllerRepresentableContext<MailView>) {}
}

